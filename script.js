var header = document.getElementById('content-header');
var content = document.getElementById('content-body');
var headerTimeout, contentTimeout;

function onDiv1Scroll(){
	//console.log('scrolling div1...');
	if(contentTimeout) return;
	if(headerTimeout) clearTimeout(headerTimeout);
	headerTimeout = setTimeout(function(){
		if(headerTimeout) {
			clearTimeout(headerTimeout);
			headerTimeout = null;
		}
	}, 50);

	content.scrollLeft = header.scrollLeft;
}

function onDiv2Scroll(){
	//console.log('scrolling div2...')
	if(headerTimeout) return;
	if(contentTimeout) clearTimeout(contentTimeout);
	contentTimeout = setTimeout(function(){
		if(contentTimeout) {
			clearTimeout(contentTimeout);
			contentTimeout = null;
		}
	}, 50);

	header.scrollLeft = content.scrollLeft;
}